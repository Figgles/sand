use ggez::event::{self, EventHandler};
use ggez::glam::*;
use ggez::graphics::{self, Color};
use ggez::input::keyboard;
use ggez::{Context, ContextBuilder, GameResult};
use rand::{
    distributions::{Distribution, Standard},
    thread_rng, Rng,
};

#[derive(Copy, Clone, PartialEq)]
enum Cell {
    Sand,
    Water,
    Empty,
}

impl Cell {
    fn color(&self) -> Color {
        match self {
            Cell::Sand => Color::YELLOW,
            Cell::Water => Color::BLUE,
            Cell::Empty => Color::BLACK,
        }
    }

    fn displaces(&self, cell: &Cell) -> bool {
	match self {
	    Cell::Sand => vec![Cell::Water, Cell::Empty],
	    _ => vec![Cell::Empty],
	}.contains(cell)
    }
}

enum Direction {
    Left,
    Right,
}

impl Distribution<Direction> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Direction {
        match rng.gen_range(0..=1) {
            1 => Direction::Right,
            _ => Direction::Left,
        }
    }
}

const BOARD_AREA: (usize, usize) = (32, 32);

const CELL_AREA: Vec2 = Vec2 { x: 16.0, y: 16.0 };

const WINDOW_AREA: Vec2 = Vec2 {
    x: (BOARD_AREA.0 as f32) * CELL_AREA.x,
    y: (BOARD_AREA.1 as f32) * CELL_AREA.y,
};

const FPS: u32 = 24;

fn main() {
    let (mut ctx, event_loop) = ContextBuilder::new("my_game", "Cool Game Author")
        .window_mode(ggez::conf::WindowMode::default().dimensions(WINDOW_AREA.x, WINDOW_AREA.y))
        .build()
        .expect("aieee, could not create ggez context!");

    let my_game = MyGame::new(&mut ctx);
    // Run!
    event::run(ctx, event_loop, my_game);
}

struct MyGame {
    board: Vec<Vec<Cell>>,
    drawing: bool,
    mouse_cell: (usize, usize),
    selected_element: Cell,
}

impl MyGame {
    pub fn new(_ctx: &mut Context) -> MyGame {
        // Load/create resources such as images here.
        MyGame {
            board: vec![vec![Cell::Empty; BOARD_AREA.0]; BOARD_AREA.1],
            drawing: false,
            mouse_cell: (0, 0),
            selected_element: Cell::Sand,
        }
    }
}

fn checked_add_below(lhs: usize, rhs: usize, limit: usize) -> Option<usize> {
    let sum = lhs + rhs;
    if sum < limit {
        Some(sum)
    } else {
        None
    }
}

impl EventHandler for MyGame {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        while ctx.time.check_update_time(FPS) {
            if self.drawing {
                self.board[self.mouse_cell.1][self.mouse_cell.0] = self.selected_element.clone();
            }

            for y in (0..BOARD_AREA.1 - 1).rev() {
                for x in 0..BOARD_AREA.0 {
                    match self.board[y][x] {
                        Cell::Sand => {
                            if Cell::Sand.displaces(&self.board[y+1][x]) {
				let displaced = self.board[y+1][x];
                                self.board[y+1][x] = Cell::Sand;
                                self.board[y][x] = displaced;
                            } else {
                                let mut rng = thread_rng();
                                let direction: Direction = rng.gen();
                                if let Some(target_x) = match direction {
                                    Direction::Left => x.checked_sub(1),
                                    Direction::Right => checked_add_below(x, 1, BOARD_AREA.0),
                                } {
                                    if Cell::Sand
					.displaces(&self.board[y + 1][target_x]) {
					let displaced = self.board[y+1][target_x];
                                        self.board[y+1][target_x] = Cell::Sand;
                                        self.board[y][x] = displaced;
                                    }
                                }
                            }
                        }
                        Cell::Water => {
                            if self.board[y + 1][x] == Cell::Empty {
                                self.board[y + 1][x] = Cell::Water;
                                self.board[y][x] = Cell::Empty;
                            } else {
                                let mut rng = thread_rng();
                                let direction: Direction = rng.gen();
                                if let Some(target_x) = match direction {
                                    Direction::Left => x.checked_sub(1),
                                    Direction::Right => checked_add_below(x, 1, BOARD_AREA.0),
                                } {
                                    if self.board[y + 1][target_x] == Cell::Empty {
                                        self.board[y + 1][target_x] = Cell::Water;
                                        self.board[y][x] = Cell::Empty;
                                    } else if self.board[y][target_x] == Cell::Empty {
					self.board[y][target_x] = Cell::Water;
                                        self.board[y][x] = Cell::Empty;
				    }
                                }
                            }
                        }
                        _ => (),
                    }
                }
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas = graphics::Canvas::from_frame(ctx, Color::WHITE);
        for (y, row) in self.board.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                canvas.draw(
                    &graphics::Mesh::new_rectangle(
                        ctx,
                        graphics::DrawMode::fill(),
                        graphics::Rect {
                            x: 0.0,
                            y: 0.0,
                            w: CELL_AREA.x,
                            h: CELL_AREA.y,
                        },
                        cell.color(),
                    )?,
                    Vec2::new((x as f32) * CELL_AREA.x, (y as f32) * CELL_AREA.y),
                );
            }
        }
        canvas.finish(ctx)
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        button: event::MouseButton,
        _x: f32,
        _y: f32,
    ) -> GameResult {
        if button == event::MouseButton::Left {
            self.drawing = true;
        }
        Ok(())
    }
    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        button: event::MouseButton,
        _x: f32,
        _y: f32,
    ) -> GameResult {
        if button == event::MouseButton::Left {
            self.drawing = false;
        }
        Ok(())
    }
    fn mouse_motion_event(
        &mut self,
        _ctx: &mut Context,
        x: f32,
        y: f32,
        _dx: f32,
        _dy: f32,
    ) -> GameResult {
        self.mouse_cell = (
            (x / CELL_AREA.x).clamp(0.0, (BOARD_AREA.0 - 1) as f32) as usize,
            (y / CELL_AREA.y).clamp(0.0, (BOARD_AREA.1 - 1) as f32) as usize,
        );
        Ok(())
    }
    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        input: keyboard::KeyInput,
        _repeat: bool,
    ) -> GameResult {
        self.selected_element = match input.keycode {
            Some(keyboard::KeyCode::S) => Cell::Sand,
            Some(keyboard::KeyCode::W) => Cell::Water,
            _ => self.selected_element.clone(),
        };
        Ok(())
    }
}
